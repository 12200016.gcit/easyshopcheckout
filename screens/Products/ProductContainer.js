import { View, Text, FlatList, StyleSheet } from "react-native";
import React, { useEffect, useState } from "react";
import ProductList from "./ProductList";
import SearchBox from "../../components/Products/SearchBox";
import Banner from "../../Shared/Banner";
const data = require("../../assets/data/products.json");
import SearchedProduct from "./SearchedProduct";
import SearchedCategory from "./SearchedCategory";


const category = require("../../assets/data/categories.json");
function ProductContainer() {
  const [products, setProducts] = useState();
  const [searchedProduct, setSearchedProduct] = useState([]);
  const [focus, setFocus] = useState(false);
  const [active, setActive] = useState(-1);

  const [categoryList, setCategoryList] = useState([]);
  useEffect(() => {
    setProducts(data);
    setCategoryList(category);
    setCategoryProducts(products);
    
    return () => {
      setProducts([]);
    };
  }, []);

  function handleSearchProducts(query) {
    setFocus(true);
    if (searchedProduct.length === 0) {
      setFocus(false);
    }
    setSearchedProduct(
      products.filter((product) =>
        product.name.toLowerCase().includes(query.toLowerCase())
      )
    );
  }

  return (
    <>
      <SearchBox onChangeText={handleSearchProducts}></SearchBox>
      {focus ? (
        <SearchedProduct searchedProduct={searchedProduct} />
      ) : (
        <View>
          <FlatList
            ListHeaderComponent={() => 
            <View>
              <View style={styles.rootContainer}>
                <Banner /></View>
              <View style={styles.rootContainer}><SearchedCategory categoryList={categoryList} />
              </View>
            <View/>
            </>
           
        }}
            ListHeaderComponentStyle={{
              paddingVertical: 15,
              backgroundColor: "grey",
              
            }}
            numColumns={2}
            data={products}
            renderItem={({ item }) => <ProductList product={item} />}
            keyExtractor={(item) => item._id.$oid}
          />
        </View>
      )}
    </>
  );
}
export default ProductContainer;

const styles = StyleSheet.create({
  
})
