import { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  Button,
} from "react-native";

const { width } = Dimensions.get("window");
function ProductCard({ product }) {
  return (
    <View style={styles.rootContainer}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={{
            uri: product.image
              ? product.image
              : "https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg",
          }}
        />
      </View>

      <Text style={styles.title}>{product.name}</Text>
      <Text style={styles.price}>Nu.{product.price.toFixed(2)}</Text>
      <Button title="add" color="green" />
      {/* <Text style={{ color: "red" }}>Currently out of stock</Text> */}

      {/* <Text>Nu. {product.price}</Text> */}
    </View>
  );
}
export default ProductCard;

const styles = StyleSheet.create({
  rootContainer: {
    width: width / 2 - 20,
    height: width / 1.7,
    backgroundColor: "white",
    padding: 10,
    marginBottom: 20,
    marginTop: 70,
    justifyContent: "center",
    alignItems: "center",
    marginhorizontal: 10,
  },
  title: {
    fontWeight: "bold",
    fontSize: 12,
    marginTop: 80,
    color: "black",
  },
  price: {
    color: "orange",
    fontSize: 16,
  },
  image: {
    top: -15,
    borderRadius: 10,
    width: "100%",
    height: "90%",
  },
  imageContainer: {
    width: width / 2 - 20 - 10,
    height: width / 2,
    top: -45,
    borderRadius: 20,
    position: "absolute",
  },
});
