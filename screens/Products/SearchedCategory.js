import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import CategoryButton from "../../components/Products/CategoryButton";

function SearchedCategory({
  categoryList,
  CategoryProducts,
  setCategoryProducts,
  active,
}) {
  return (
    <ScrollView horizontal={true}>
      <View style={styles.rootContainer}>
        <CategoryButton
          onPress={() => {
            setCategoryProducts(CategoryProducts);
            setActive(-1);
          }}
          style={active ? styles.activeColor : null}
        >
          All
        </CategoryButton>

        {categoryList.map((category) => (
          <CategoryButton key={category.name}>{category.name}</CategoryButton>
        ))}
      </View>
    </ScrollView>
  );
}

export default SearchedCategory;
const styles = StyleSheet.create({
  rootContainer: {
    flexDirection: "row",
    backgroundColor: "white",
    paddingVertical: 10,
    paddingLeft: 10,
  },
});
