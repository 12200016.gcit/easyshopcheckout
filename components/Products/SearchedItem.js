import { View, Text, StyleSheet, Image } from "react-native";
import React from "react";

export default function SearchedItem({ product }) {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          resizeMode="contain"
          source={{
            uri: product.image
              ? product.image
              : "https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg",
          }}
        />
      </View>
      <View style={styles.desctiption}>
        <Text>{product.name}</Text>
        <Text>{product.description}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    // backgroundColor: "grey",
    padding: 10,
    margin: 4,
  },
  image: {
    width: "100%",
    height: "100%",
    // display: "contain",
  },
  imageContainer: {
    flex: 4,
    height: 100,
    width: "100%",
    borderRadius: 15,
    // width:
    // flexDirection: "row",
  },
  desctiption: {
    padding: 20,
    flex: 10,
  },
});
