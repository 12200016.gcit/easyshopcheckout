import React, { Children } from "react";
import { Pressable, Text } from "react-native";

function CategoryButton({ Children, onPress, styles }) {
  return (
    <Pressable style={{styles.rootContainer, style}} onPress={onPress}>
      <Text>{Children}</Text>
    </Pressable>
  );
}

export default CategoryButton;
const styles = StyleSheet.create({
  rootContainer: {
    backgroundColor: "blue",
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginRight: 10,
  },
  text: {
    color: "white",
    fontSize: 16,
  },
});
